import { range, Observer } from "rxjs";

const observer: Observer<number> = {
    next(value: number): void {
        console.log(`this is next(): ${value}`)
    },
    complete(): void {
        console.log(`this is complete()`)
    },
    error(err: Error): void {
        console.log(`this is error(): ${err}`)
    }
};

function main() {
    let observable = range(1, 2);
    observable.subscribe(observer);
    observable = range(1,3);
    observable.subscribe((observer));
}

main();